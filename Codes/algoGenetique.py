## ALGORITHME GENETIQUE - SIMULATEUR INCENDIE - VIDAL ATTIAS
import random as rd
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib
import time
import numpy as np
import math
import pickle
N = 100

class Interface:
    def __init__(self, N,Rayon1, Rayon2, show = True, session=0):
        self.n = N
        self.session = session
        self.save = False
        self.iteration = 0
        self.tableauCalculs = []
        self.Rayon1 = Rayon1
        self.Rayon2 = Rayon2
        
        
        self.couleursSol = ['#006600','#A76726','#00ccff','#B4AF91','#000000'] #Herbe Arbre Eau Roche Brulé
        self.couleursFeu = ['#00ad5e','#dbdc01','#FFA500','#9a0000','#000000'] #Vert Jaune Orange Rouge Brulé
        
        self.matFeu = [[0 for i in range(N)]for j in range(N)]
        self.matSol ,self.matHygro ,self.matCombustible = self.generer(Rayon1, Rayon2)



                    
        self.feu = Feu(self)
        if show:
            self.cmapSol = matplotlib.colors.ListedColormap(self.couleursSol)
            self.cmapFeu = matplotlib.colors.ListedColormap(self.couleursFeu)
    
            bounds=[0,1,2,3,4,5]
            self.norm = matplotlib.colors.BoundaryNorm(bounds, self.cmapSol.N)
            self.norm = matplotlib.colors.BoundaryNorm(bounds, self.cmapFeu.N)
    
            # self.fig = plt.figure(0,figsize=(2000, 1200))
            self.fig = plt.figure(0)
            self.ax = []
    
            titre = ['Combustile Terrain','Nature du terrain','Hygrométrie','État du feu','Histogramme']
            for i in range(4):
                self.ax.append(self.fig.add_subplot(2,2,i, aspect='equal'))
                self.ax[i].set_title(titre[i])
                
                
    ## SOL           
            im1 = self.ax[1].matshow(self.matSol, cmap=self.cmapSol, norm=self.norm)
            self.fig.colorbar(im1,ax=self.ax[1])
            self.ax[1].axis('off')
    
    ## FEU
            im2 = self.ax[3].matshow(self.matFeu, cmap=plt.cm.YlOrRd)
            self.fig.colorbar(im2,ax=self.ax[3])
            self.ax[3].axis('off')
    
    ## HYGRO
            im3 = self.ax[2].matshow(self.matHygro, cmap=plt.cm.Blues)
            self.fig.colorbar(im3,ax=self.ax[2])
            self.ax[2].axis('off')
            
    ## COMBUSTIBLE
            im4 = self.ax[0].matshow(self.matCombustible, cmap=plt.cm.hot)
            self.fig.colorbar(im4,ax=self.ax[0])
            self.ax[0].axis('off')
        
    def generer(self, R1,R2):
        mat = [[0 for i in range(self.n)] for k in range(self.n)]
        matH = [[rd.randint(0,100) for i in range(self.n)] for k in range(self.n)]
        matC = [[rd.randint(70,90) for i in range(self.n)] for k in range(self.n)]
        ##FORET
        x = int(self.n/2)
        for i in range(self.n):
            for j in range(self.n):
                if np.sqrt((x-i)**2+(x-j)**2)<R2 and np.sqrt((x-i)**2+(x-j)**2)>=R1:
                    mat[i][j] = 1
                    matH[i][j] = rd.randint(40,60)
                    matC[i][j] = rd.randint(60,70)
                if np.sqrt((x-i)**2+(x-j)**2)<R1:
                    mat[i][j] = 3
                    matH[i][j] = rd.randint(0, 20)
                    matC[i][j] = rd.randint(80,100)
            mat[0][0] = 0
            matC[0][0] = 0
            matH[0][0] = 100
        return mat, matH, matC

    def start(self,show=True):
        ##PREMIERE ETAPE
        self.matFeuTemp = []
        self.calculs = 0
        for i in range(self.n):
            matTemp = []
            for j in range(self.n):
                matTemp.append([self.matFeu[i][j], True])
            self.matFeuTemp.append(matTemp)

        ##SECONDE ETAPE
        #On effectue la propagation du feu et on reduit matCombustible et matHygro
        for i in range(self.n):
            for j in range(self.n):
                if self.matFeu[i][j]>1:
                    self.calculs += 1
                    self.feu.propagerFeu1([i,j], self)
                self.matCombustible[i][j] = max([0, self.matCombustible[i][j]-self.matFeuTemp[i][j][0]])
                self.matHygro[i][j] = max([0, self.matHygro[i][j]-self.matFeuTemp[i][j][0]/2])
        ##TROISIEME ETAPE        
        #Ici on remplace matFeu par le nouveau matFeuTemp
        self.matFeu = []
        for i in range(self.n):
            matTemp = []
            for j in range(self.n):
                if self.matCombustible[i][j]<10 and self.matFeuTemp[i][j][0]!=0: #Ici on réduit le feu si combustible <10
                    self.calculs += 1
                    self.matFeuTemp[i][j][0] = max([0, self.matFeuTemp[i][j][0]-0.3]) 
                matTemp.append(self.matFeuTemp[i][j][0])
            self.matFeu.append(matTemp)
        
        #On dessine et on itère
        if show==True:
            self.dessinerMatrice()
        self.tableauCalculs.append(self.calculs)
        self.iteration += 1

    def surface(self):
        surface = 0
        for i in range(self.n):
            for j in range(self.n):
                if self.matCombustible[i][j]<10:
                    surface+=1
        return surface

    def dessinerMatrice(self):
        self.im1 = self.ax[1].matshow(self.matSol, cmap=self.cmapSol, norm=self.norm)
        self.im2 = self.ax[3].matshow(self.matFeu, cmap=plt.cm.YlOrRd)
        self.im3 = self.ax[2].matshow(self.matHygro, cmap=plt.cm.Blues)
        self.im4 = self.ax[0].matshow(self.matCombustible, cmap=plt.cm.hot)
            
        plt.pause(0.1)
        #On sauvegarde
        if self.save:
            self.fig.savefig('/Users/vidalattias/Desktop/simulateur/images/images '+str(self.session)+'/'+str(self.iteration)+'_R1='+str(self.Rayon1)+'_R2='+str(self.Rayon2)+'.png')


class Feu:
    def __init__(self, i):
        self.Feu = []
        x = int(i.n/2)
        y = int(i.n/2)
        self.Feu.append([x, y])
        i.matFeu[x][y] = 3

    def propagerFeu1(self, c, i):  ##i : interface
        feuCase = i.matFeuTemp[c[0]][c[1]]
        intens = (feuCase[0] - 1) * 0.4 + 0.1
        if c[0] > 0:
            choix = rd.random()
            if choix < 1 - i.matHygro[c[0] - 1][c[1]] / 100 and i.matFeuTemp[c[0] - 1][c[1]][1] == True:
                feuMax = i.matCombustible[c[0] - 1][c[1]] * (100 - i.matHygro[c[0]][c[1]]) * 3 / 10000

                i.matFeuTemp[c[0] - 1][c[1]][0] = min(i.matFeuTemp[c[0] - 1][c[1]][0] + intens, feuMax)
                i.matFeuTemp[c[0] - 1][c[1]][1] = False

        if c[0] < i.n - 1:
            choix = rd.random()
            if choix < 1 - i.matHygro[c[0] + 1][c[1]] / 100 and i.matFeuTemp[c[0] + 1][c[1]][1] == True:
                feuMax = i.matCombustible[c[0] + 1][c[1]] * (100 - i.matHygro[c[0]][c[1]]) * 3 / 10000

                i.matFeuTemp[c[0] + 1][c[1]][0] = min(i.matFeuTemp[c[0] + 1][c[1]][0] + intens, feuMax)
                i.matFeuTemp[c[0] + 1][c[1]][1] = False

        if c[1] > 0:
            choix = rd.random()
            if choix < 1 - i.matHygro[c[0]][c[1] - 1] / 100 and i.matFeuTemp[c[0]][c[1] - 1][1] == True:
                feuMax = i.matCombustible[c[0]][c[1] - 1] * (100 - i.matHygro[c[0]][c[1]]) * 3 / 10000

                i.matFeuTemp[c[0]][c[1] - 1][0] = min(i.matFeuTemp[c[0]][c[1] - 1][0] + intens, feuMax)
                i.matFeuTemp[c[0]][c[1] - 1][1] = False

        if c[1] < i.n - 1:
            choix = rd.random()
            if choix < 1 - i.matHygro[c[0]][c[1] + 1] / 100 and i.matFeuTemp[c[0]][c[1] + 1][1] == True:
                feuMax = i.matCombustible[c[0]][c[1] + 1] * (100 - i.matHygro[c[0]][c[1]]) * 3 / 10000

                i.matFeuTemp[c[0]][c[1] + 1][0] = min(i.matFeuTemp[c[0]][c[1] + 1][0] + intens, feuMax)
                i.matFeuTemp[c[0]][c[1] + 1][1] = False
surface = []
R1 = []
R2 = []
Show = True
rayon1 = rd.randint(0, int(N/2))
for k in range(10):
    print("##SESSION = "+str(k))
    rayon2 = rd.randint(rayon1, int(N/2))
    R1.append(rayon1)
    R2.append(rayon2)
    plt.close()
    run = Interface(N,rayon1,rayon2, show = Show, session=k)
    nn = 200
    a = 0
    while (a < nn):
        a += 1
        if a % 20 == 0:
            print(a)
        run.start(show=Show)
    run.start(show=Show)
    surface.append(run.surface())