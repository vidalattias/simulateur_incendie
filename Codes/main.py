#!/usr/bin/env python
# -*- coding: utf-8 -*-
## MAIN - SIMULATEUR INCENDIE - VIDAL ATTIAS
import matplotlib
import random as rd
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib
import time
import numpy as np
import math
import pickle

nn = 200

exec(open('/Users/vidalattias/Desktop/simulateur/feu.py').read())



with open('/Users/vidalattias/Desktop/simulateur/mat.pkl', 'rb') as pickle_load:
    mat = pickle.load(pickle_load)

with open('/Users/vidalattias/Desktop/simulateur/matH.pkl', 'rb') as pickle_load:
    matH = pickle.load(pickle_load)

with open('/Users/vidalattias/Desktop/simulateur/matC.pkl', 'rb') as pickle_load:
    matC = pickle.load(pickle_load)

class Interface:
    def __init__(self, N, mat, matH, matC, show = True):
        self.n = 50
        self.K = 3
        self.save = False
        self.iteration = 5
        self.tableauCalculs = []
        
        
        self.couleursSol = ['#006600','#A76726','#00ccff','#B4AF91','#000000'] #Herbe Arbre Eau Roche Brulé
        self.couleursFeu = ['#00ad5e','#dbdc01','#FFA500','#9a0000','#000000'] #Vert Jaune Orange Rouge Brulé
        
        self.matFeu = [[0 for i in range(N)]for j in range(N)]
        self.matSol = mat
        self.matHygro = matH
        self.matCombustible = matC
                    
        self.feu = Feu(self)
        if show:
            self.cmapSol = matplotlib.colors.ListedColormap(self.couleursSol)
            self.cmapFeu = matplotlib.colors.ListedColormap(self.couleursFeu)
    
            bounds=[0,1,2,3,4,5]
            self.norm = matplotlib.colors.BoundaryNorm(bounds, self.cmapSol.N)
            self.norm = matplotlib.colors.BoundaryNorm(bounds, self.cmapFeu.N)
    
            # self.fig = plt.figure(0,figsize=(2000, 1200))
            self.fig = plt.figure(0)
            self.ax = []
    
            titre = ['Combustile Terrain','Nature du terrain','Hygrométrie','État du feu','Histogramme']
            for i in range(4):
                self.ax.append(self.fig.add_subplot(2,2,i, aspect='equal'))
                self.ax[i].set_title(titre[i])
                
                
    ## SOL           
            im1 = self.ax[1].matshow(self.matSol, cmap=self.cmapSol, norm=self.norm)
            self.fig.colorbar(im1,ax=self.ax[1])
            self.ax[1].axis('off')
    
    ## FEU
            im2 = self.ax[3].matshow(self.matFeu, cmap=plt.cm.YlOrRd)
            self.fig.colorbar(im2,ax=self.ax[3])
            self.ax[3].axis('off')
    
    ## HYGRO
            im3 = self.ax[2].matshow(self.matHygro, cmap=plt.cm.Blues)
            self.fig.colorbar(im3,ax=self.ax[2])
            self.ax[2].axis('off')
            
    ## COMBUSTIBLE
            im4 = self.ax[0].matshow(self.matCombustible, cmap=plt.cm.hot)
            self.fig.colorbar(im4,ax=self.ax[0])
            self.ax[0].axis('off')
        
        
    def start(self,show=True):
        ##PREMIERE ETAPE
        self.matFeuTemp = []
        self.calculs = 0
        for i in range(self.n):
            matTemp = []
            for j in range(self.n):
                matTemp.append([self.matFeu[i][j], True])
            self.matFeuTemp.append(matTemp)

        ##SECONDE ETAPE
        #On effectue la propagation du feu et on reduit matCombustible et matHygro
        for i in range(self.n):
            for j in range(self.n):
                if self.matFeu[i][j]>1:
                    self.calculs += 1
                    self.feu.propagerFeu1([i,j], self)
                self.matCombustible[i][j] = max([0, self.matCombustible[i][j]-self.matFeuTemp[i][j][0]])
                self.matHygro[i][j] = max([0, self.matHygro[i][j]-self.matFeuTemp[i][j][0]/2])
        ##TROISIEME ETAPE        
        #Ici on remplace matFeu par le nouveau matFeuTemp
        self.matFeu = []
        for i in range(self.n):
            matTemp = []
            for j in range(self.n):
                if self.matCombustible[i][j]<10 and self.matFeuTemp[i][j][0]!=0: #Ici on réduit le feu si combustible <10
                    self.calculs += 1
                    self.matFeuTemp[i][j][0] = max([0, self.matFeuTemp[i][j][0]-0.3]) 
                matTemp.append(self.matFeuTemp[i][j][0])
            self.matFeu.append(matTemp)
        
        #On dessine et on itère
        if show==True:
            self.dessinerMatrice()
        self.tableauCalculs.append(self.calculs)
        self.iteration += 1



    def dessinerMatrice(self):
        self.im1 = self.ax[1].matshow(self.matSol, cmap=self.cmapSol, norm=self.norm)
        self.im2 = self.ax[3].matshow(self.matFeu, cmap=plt.cm.YlOrRd)
        self.im3 = self.ax[2].matshow(self.matHygro, cmap=plt.cm.Blues)
        self.im4 = self.ax[0].matshow(self.matCombustible, cmap=plt.cm.hot)
            
        plt.pause(0.1)
        #On sauvegarde
        if self.save:
            self.fig.savefig('/Users/vidalattias/Desktop/simulateur/'+str(self.K)+'/'+str(self.iteration)+'.png')
            
nn = 300
tabrun = []
tabTemps = []
for simul in range(5):
    print(simul)
    with open('/Users/vidalattias/Desktop/simulateur/mat.pkl', 'rb') as pickle_load:
        mat = pickle.load(pickle_load)
    
    with open('/Users/vidalattias/Desktop/simulateur/matH.pkl', 'rb') as pickle_load:
        matH = pickle.load(pickle_load)
    
    with open('/Users/vidalattias/Desktop/simulateur/matC.pkl', 'rb') as pickle_load:
        matC = pickle.load(pickle_load)
    tabrun.append(Interface(50, mat, matH, matC, show = False))
    tDepart = time.time()
    tableauTemps = []
    tableauDuree = []
    a = 0
    tableauFeu = [tabrun[simul].matFeu]
    while(a<nn):
        a+=1
        if a%20==0:
            print(a)
        t1 = time.time()
        tabrun[simul].start(show=False)
        t2 = time.time()
        tableauTemps.append(t2-t1)
        tableauDuree.append(t2-tDepart)
        tableauFeu.append(tabrun[simul].matFeu)
        
    tabTemps.append(tableauTemps)
    plt.plot(tableauTemps)
    plt.savefig('/Users/vidalattias/Desktop/simulateur/temps/temps_'+str(simul)+'.png')
    plt.close()
    plt.plot(tabrun[simul].tableauCalculs)
    plt.savefig('/Users/vidalattias/Desktop/simulateur/tableauCalculs/calculs'+str(simul)+'.png')
    plt.close()
    
def moyenneListe(L):
    longueur = len(L[0])
    n = len(L)
    moy = []
    for i in range(longueur):
        somme = 0
        for liste in L:
            somme+=liste[i]
        moy.append(somme/n)
    return moy
    
L = []
for i in tabrun:
    L.append(i.tableauCalculs)
 
    
moy = moyenneListe(L)
plt.plot(moy)
# plt.savefig('/Users/vidalattias/Desktop/simulateur/tableauCalculs/moyenne.png')
plt.close()

L = []
for i in tabTemps:
    L.append(i)
    
moy = moyenneListe(L)
plt.plot(moy)
plt.pause(1)
# plt.savefig('/Users/vidalattias/Desktop/simulateur/temps/moyenne.png')
plt.close()
