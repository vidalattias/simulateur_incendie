import random as rd
N = 50
mat = [[0 for i in range(50)] for k in range(50)]

for i in range(50):
    for k in range(5):
        mat[i][min(30+k+(int(i/10)),49)] = 2
        

    for k in range(30):
        x = min(14+k,38)
        y = min(14+int(k/10),48)
        for i in range(11):
            for j in range(11):
                if(np.sqrt(i**2+j**2)<=11):
                    mat[x+i][y+j] = 1
                    mat[x-i][y+j] = 1
                    mat[x+i][y-j] = 1
                    mat[x-i][y-j] = 1
    for k in range(15):
        x = 15+k
        y = 14
        for i in range(10):
            for j in range(10):
                if(np.sqrt(i**2+j**2)<=7):
                    mat[x+i][y+j] = 3
                    mat[x-i][y+j] = 3
                    mat[x+i][y-j] = 3
                    mat[x-i][y-j] = 3

##   HYGRO

matH = [[100*rd.random() for i in range(50)] for k in range(50)]

for i in range(50):
    for k in range(5):
        matH[i][min(30+k+(int(i/10)),49)] = 100
        

    for k in range(30):
        x = min(14+k,38)
        y = min(14+int(k/10),48)
        ## FORET HUMIDE
        for i in range(11):
            for j in range(11):
                norme = np.sqrt(i**2+j**2)
                if(norme<=11):
                    matH[x+i][y+j] = rd.randint(40, 60)
                    matH[x-i][y+j] = rd.randint(40, 60)
                    matH[x+i][y-j] = rd.randint(40, 60)
                    matH[x-i][y-j] = rd.randint(40, 60)
    for k in range(15):
        x = 15+k
        y = 14
        ##FORET SECHE
        for i in range(10):
            for j in range(10):
                norme = np.sqrt(i**2+j**2)
                if(norme<=7):
                    matH[x+i][y+j] = rd.randint(0, 20)
                    matH[x-i][y+j] = rd.randint(0, 20)
                    matH[x+i][y-j] = rd.randint(0, 20)
                    matH[x-i][y-j] = rd.randint(0, 20)
                    
                    
matCombustible = [[0 for i in range(N)] for j in range(N)]
for i in range(N):
    for j in range(N):
        if mat[i][j]==0:
            matCombustible[i][j] = rd.randint(70,90)
        if mat[i][j]==1:
            matCombustible[i][j] = 100
        if mat[i][j]==2:
            matCombustible[i][j] = 0
        if mat[i][j]==3:
            matCombustible[i][j] = rd.randint(80,100)

import pickle

with open('mat.pkl', 'wb') as pickle_file:
    pickle.dump(mat, pickle_file, protocol=pickle.HIGHEST_PROTOCOL)
    
with open('matH.pkl', 'wb') as pickle_file:
    pickle.dump(matH, pickle_file, protocol=pickle.HIGHEST_PROTOCOL)
    
with open('matC.pkl', 'wb') as pickle_file:
    pickle.dump(matCombustible, pickle_file, protocol=pickle.HIGHEST_PROTOCOL)