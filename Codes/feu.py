## Feu - SIMULATEUR INCENDIE - VIDAL ATTIAS
import random as rd

class Feu:
    
    def __init__(self, i):
        self.Feu = []
        x = 23
        y = 10
        self.Feu.append([x,y])
        i.matFeu[x][y] = 3
        
    def propagerFeu1(self,c, i): ##i : interface
        feuCase = i.matFeuTemp[c[0]][c[1]]
        intens = (feuCase[0]-1)*0.4+0.1
        if c[0]>0:
            choix = rd.random()
            if choix<1-i.matHygro[c[0]-1][c[1]]/100 and i.matFeuTemp[c[0]-1][c[1]][1] == True:
                feuMax = i.matCombustible[c[0]-1][c[1]]*(100-i.matHygro[c[0]][c[1]])*3/10000
                
                i.matFeuTemp[c[0]-1][c[1]][0] = min(i.matFeuTemp[c[0]-1][c[1]][0] + intens , feuMax)
                i.matFeuTemp[c[0]-1][c[1]][1] = False
                
                
                
        if c[0]<i.n-1:
            choix = rd.random()
            if choix<1-i.matHygro[c[0]+1][c[1]]/100 and i.matFeuTemp[c[0]+1][c[1]][1] == True:
                feuMax = i.matCombustible[c[0]+1][c[1]]*(100-i.matHygro[c[0]][c[1]])*3/10000 
                
                i.matFeuTemp[c[0]+1][c[1]][0] = min(i.matFeuTemp[c[0]+1][c[1]][0] + intens , feuMax)
                i.matFeuTemp[c[0]+1][c[1]][1] = False



        if c[1]>0:
            choix = rd.random()
            if choix<1-i.matHygro[c[0]][c[1]-1]/100 and i.matFeuTemp[c[0]][c[1]-1][1] == True:
                feuMax = i.matCombustible[c[0]][c[1]-1]*(100-i.matHygro[c[0]][c[1]])*3/10000
                
                i.matFeuTemp[c[0]][c[1]-1][0] = min(i.matFeuTemp[c[0]][c[1]-1][0] + intens , feuMax)
                i.matFeuTemp[c[0]][c[1]-1][1] = False
                


        if c[1]<i.n-1:
            choix = rd.random()
            if choix<1-i.matHygro[c[0]][c[1]+1]/100 and i.matFeuTemp[c[0]][c[1]+1][1] == True:
                feuMax = i.matCombustible[c[0]][c[1]+1]*(100-i.matHygro[c[0]][c[1]])*3/10000
                
                i.matFeuTemp[c[0]][c[1]+1][0] = min(i.matFeuTemp[c[0]][c[1]+1][0] + intens , feuMax)
                i.matFeuTemp[c[0]][c[1]+1][1] = False
    
    
    
    
    
    def propagerFeu2(self,c, i):
        gradFeu = 0
        gradHygro = 0
        k = 0
        if c[0]>0:
            gradFeu += i.matFeu[c[0]-1][c[1]]-i.matFeu[c[0]][c[1]]
            gradHygro += i.matHygro[c[0]-1][c[1]]-i.matHygro[c[0]][c[1]]
            k+=1
        if c[1]>0:
            gradFeu += i.matFeu[c[0]][c[1]-1]-i.matFeu[c[0]][c[1]]
            gradHygro += i.matHygro[c[0]][c[1]-1]-i.matHygro[c[0]][c[1]]
            k+=1
        if c[0]<i.n-1:
            gradFeu += i.matFeu[c[0]+1][c[1]]-i.matFeu[c[0]][c[1]]
            gradHygro += i.matHygro[c[0]+1][c[1]]-i.matHygro[c[0]][c[1]]
            k+=1
        if c[1]<i.n-1:
            gradFeu += i.matFeu[c[0]][c[1]+1]-i.matFeu[c[0]][c[1]]
            gradHygro += i.matHygro[c[0]][c[1]+1]-i.matHygro[c[0]][c[1]]
            k+=1
        
        gradFeu = gradFeu/k
        gradHygro = gradHygro/k
        
        if gradFeu > 0:
            print('tes')
